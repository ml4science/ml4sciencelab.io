Configuration files
===================

In order to make the framework easy to understand and allow the users to tune the parameters, the software uses `YAML file <https://www.cloudbees.com/blog/yaml-tutorial-everything-you-need-get-started/>`_ to store customizable variables to be used during training or even for other operations which may require user-defined parameters. In this page, we list all the parameters that can be used:

Training files
--------------

There are 4 main top-level entries that are required in the configuration file when doing training:

- ``trainer``: This specifies the type of training to be used (at the moment only generic type is available).
- ``output_dir``: Where to store the results of the training.
- ``data_config``: Information regarding dataset to be used.
- ``model_config``: Information regarding the neural network model to be used.
- ``train_config``: Details of the training.
