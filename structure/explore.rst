Explore modules
===============

All functions listed in this page can be used to do a variety of operations. We have separated the functions into multiple modules.

Standard I/O routines
---------------------

.. currentmodule:: mldas.explore.loading

.. autosummary::
   :toctree: generated/

   hdf5read
   load_model
   save_data
   load_image
   load_bulk

DAS data loader
---------------

.. currentmodule:: mldas.explore.prepare

.. autosummary::
   :toctree: generated/

   set_from_file
   mean_shift
   set_creation
   prepare_loader

Data Distribution
-----------------

.. currentmodule:: mldas.explore.distrib

.. autosummary::
   :toctree: generated/

   gauss_single
   gauss_double
   distfit

Evaluation
----------

.. currentmodule:: mldas.explore.evaluation

.. autosummary::
   :toctree: generated/

   decode_plot
   embedding_plot
   epoch_recon
   label_2d_latent
   success_rate
   
Figures
-------

.. currentmodule:: mldas.explore.figures

.. autosummary::
   :toctree: generated/

   plot_prob_map
   plot_map_stage

Mapping
-------

.. currentmodule:: mldas.explore.mapping

.. autosummary::
   :toctree: generated/

   minute_prob
   minute_prob_test
   extract_prob_map

Cross-correlation
-----------------

.. currentmodule:: mldas.explore.xcorr

.. autosummary::
   :toctree: generated/

   avg_fft
   xcorr_freq

Phase-weighted stack
--------------------

.. currentmodule:: mldas.explore.stacking

.. autosummary::
   :toctree: generated/

   phase_weight_stack
   ts_weighting
   
Single
------

.. currentmodule:: mldas.explore.single

.. autosummary::
   :toctree: generated/

   suplearn_simple


