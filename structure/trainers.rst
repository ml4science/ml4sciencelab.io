Deep Learning trainers
======================

Base trainer class
------------------

.. currentmodule:: mldas.trainers.base

.. autoclass:: BaseTrainer
   :no-inherited-members:

     None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__init__
      ~print_model_summary
      ~save_summary
      ~write_summaries
      ~write_checkpoint
      ~train

   .. rubric:: Methods Documentation

   .. automethod:: __init__
   .. automethod:: print_model_summary
   .. automethod:: save_summary
   .. automethod:: write_summaries
   .. automethod:: write_checkpoint
   .. automethod:: train
		   
Classifier training
-------------------

.. currentmodule:: mldas.trainers.generic

.. autoclass:: GenericTrainer
   :no-inherited-members:

     None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__init__
      ~build_model
      ~train_epoch
      ~evaluate
      ~accuracy

   .. rubric:: Methods Documentation

   .. automethod:: __init__
   .. automethod:: build_model
   .. automethod:: train_epoch
   .. automethod:: evaluate
   .. automethod:: accuracy
