# Makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
BUILDDIR      = _build
# Internal variables.
ALLSPHINXOPTS   = -d $(BUILDDIR)/doctrees $(PAPEROPT_$(PAPER)) $(SPHINXOPTS) .

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)/
	find . -type f -name '.DS_Store' -print -exec rm {} \;

.PHONY: html
html:
	rm -rf $(BUILDDIR)/
	sphinx-autogen -o generated *.rst
	$(SPHINXBUILD) -b html $(ALLSPHINXOPTS) $(BUILDDIR)
	cp -r google334ace2f23267058.html mldas $(BUILDDIR)/
	python schema.py && mv $(BUILDDIR)/tmp.html $(BUILDDIR)/index.html
	@echo
	@echo "Build finished. The HTML pages are in $(BUILDDIR)."
	rm -rf structure/generated/
