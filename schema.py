structured_data="""
<script type="application/ld+json">
  {
      "@context": "https://schema.org/",
      "@type": "CreativeWork",
      "@id": "https://ml4science.gitlab.io/",
      "about": "A Machine Learning Software for Distributed Acoustic Sensing Data.",
      "author": {
          "@type": "Person",
          "name": "Vincent Dumont",
          "url": "https://vincentdumont.gitlab.io/",
          "email": "vincentdumont11@gmail.com",
          "sameAs": [
              "https://crd.lbl.gov/departments/data-science-and-technology/sdm/staff/vincent-dumont",
              "https://crd.lbl.gov/departments/applied-mathematics/ccse/staff-and-postdocs/vincent-dumont",
              "https://www.linkedin.com/in/vadumont/",
              "https://orcid.org/0000-0002-4718-1051",
              "https://ui.adsabs.harvard.edu/public-libraries/tv-WqUNSTHWfFr7cvNSlvw",
              "https://arxiv.org/a/dumont_v_1.html",
              "https://scholar.google.com/citations?user=xc3Xe1kAAAAJ",
              "https://www.researchgate.net/profile/Vincent_Dumont",
              "https://pypi.org/user/vdumont/",
              "https://zenodo.org/search?page=1&size=100&q=%22Vincent,%20Dumont%22",
              "https://github.com/vincentdumont",
              "https://gitlab.com/vincentdumont",
              "https://www.flickr.com/photos/astrodumont/",
              "https://www.instagram.com/astrodumont/",
              "https://www.youtube.com/channel/UCzJWYTknb5U6wQDgz55FVCQ",
              "http://www.wikidata.org/entity/Q62519966"
          ]
      }
  }
</script>
"""
index=open('_build/index.html','r')
tmp=open('_build/tmp.html','w')
for line in index:
    if '</head>' in line:
        tmp.write(structured_data)
    tmp.write(line)
index.close()
tmp.close()
