Machine Learning training
-------------------------

In order to perform both single-node and distributed CPU (or GPU) training effectively, we repurposed `this Pytorch benchmark repository <https://github.com/sparticlesteve/pytorch-benchmarks>`_, originally developed by Steven Farrell at NERSC and used to do `machine learning benchmarking <https://docs.nersc.gov/analytics/machinelearning/benchmarks/>`_ on the Cori cluster, and slightly modified its content to make it work with the neural network developed for DAS data analysis.
