Python executables
==================

Trainers
--------

.. currentmodule:: das_train

.. autosummary::
   :toctree: generated/

   parse_args
   load_config
   main

Result Gathering
----------------

.. currentmodule:: das_gather

.. autosummary::
   :toctree: generated/

   parse_args
   load_data
   get_model
   get_prob
   get_prob_from_file
   prob_check
   get_file_results
   make_subplot
   add_legend
   plot_results
   main
